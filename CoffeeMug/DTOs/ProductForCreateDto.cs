using System;
using System.ComponentModel.DataAnnotations;

namespace CoffeeMug.DTOs
{
    public class ProductForCreateDto
    {
        [Required]
        [MaxLength(15, ErrorMessage = "Maximum length is 15")]
        public string Name { get; set; }
        [Required]
        public Decimal Price { get; set; }
    }
}