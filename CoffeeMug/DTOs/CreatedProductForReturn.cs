namespace CoffeeMug.DTOs
{
    public class CreatedProductForReturn
    {
        public string ProductID { get; set; }

        public CreatedProductForReturn(string guid)
        {
            ProductID = guid;
        }
    }
}