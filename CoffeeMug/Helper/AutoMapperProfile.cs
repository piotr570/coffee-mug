using AutoMapper;
using CoffeeMug.DTOs;
using CoffeeMug.Model;

namespace CoffeeMug.Helper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ProductForCreateDto, Product>();
            CreateMap<ProductForUpdateDto, Product>();
        }
    }
}