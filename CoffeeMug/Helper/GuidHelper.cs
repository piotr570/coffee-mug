using System;

namespace CoffeeMug.Helper
{
    public class GuidHelper
    {
        public static bool ValidateGuid(string guid)
        {
            return ValidateIfEmpty(guid) && ValidateIfGuid(guid);
        }

        private static bool ValidateIfGuid(string guid)
        {
            Guid parsedGuid;
            return Guid.TryParse(guid, out parsedGuid);
        }

        private static bool ValidateIfEmpty(string guid)
        {
            return !string.IsNullOrEmpty(guid) || !string.IsNullOrWhiteSpace(guid);
        }
    }
}