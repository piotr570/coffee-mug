using System;
using System.ComponentModel.DataAnnotations;

namespace CoffeeMug.Model
{
    public class Product
    {
        public Guid Id { get; set; } 
        [MaxLength(15)]
        public string Name { get; set; }
        public Decimal Price { get; set; }

        public Product(string name, Decimal price)
        {
            Name = name;
            Price = price;
        }
    }
}