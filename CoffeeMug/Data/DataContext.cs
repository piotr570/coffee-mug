using CoffeeMug.Model;
using Microsoft.EntityFrameworkCore;

namespace CoffeeMug.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
        
        public DbSet<Product> Products { get; set; }
    }
}