using System.Collections.Generic;
using System.Threading.Tasks;
using CoffeeMug.Model;

namespace CoffeeMug.Data.Repositories.Interfaces
{
    public interface IRepository
    {
        Task<List<Product>> GetAll();
        Task<Product> GetByGuid(string id);
        void AddNewRecord<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<bool> DeleteByGuid(string guid);
    }
}