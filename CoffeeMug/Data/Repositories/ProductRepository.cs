using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoffeeMug.Data.Repositories.Interfaces;
using CoffeeMug.Model;
using Microsoft.EntityFrameworkCore;

namespace CoffeeMug.Data.Repositories
{
    public class ProductRepository : IRepository
    {
        private DataContext _context;

        public ProductRepository(DataContext context)
        {
            _context = context;
        }
        
        public async Task<List<Product>> GetAll()
        {
            if (!_context.Products.Any()) return null;
            var productsToReturn = await _context.Products.ToListAsync();
            return productsToReturn;
        }
        
        public async Task<Product> GetByGuid(string guid)
        {
            if (!Guid.TryParse(guid, out var parsedGuid)) return null;
            var productToReturn = await _context.Products.FirstOrDefaultAsync(x => x.Id == parsedGuid);
            return productToReturn;
        }

        public async void AddNewRecord<T>(T entity) where T : class
        {
            await _context.AddAsync(entity);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteByGuid(string guid)
        {
            var productFromRepo = await GetByGuid(guid);

            if (productFromRepo == null) return false;
            _context.Remove(productFromRepo);
            return true;
        }
    }
}