using System.Collections.Generic;
using CoffeeMug.Model;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;

namespace CoffeeMug.Data
{
    public class Seed
    {
        public static void SeedProducts(DataContext context)
        {
            if (!context.Products.Any())
            {
                var productData = System.IO.File.ReadAllText("Data/ProductSeedData.json");
                var products = JsonConvert.DeserializeObject<List<Product>>(productData);
                foreach (var product in products)
                {
                    context.Products.Add(product);
                }

                context.SaveChanges();
            }
        }
    }
}