using System;
using System.Threading.Tasks;
using AutoMapper;
using CoffeeMug.Data.Repositories.Interfaces;
using CoffeeMug.DTOs;
using CoffeeMug.Helper;
using CoffeeMug.Model;
using Microsoft.AspNetCore.Mvc;

namespace CoffeeMug.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : Controller
    {
        private readonly IRepository _repo;
        private readonly IMapper _mapper;

        public ProductsController(IRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _repo.GetAll();
            if (result != null) return Ok(result);
            return BadRequest("An error occured during operation");
        }

        [HttpGet("{guid}")]
        public async Task<IActionResult> Get(string guid)
        {
            if (GuidHelper.ValidateGuid(guid))
                return BadRequest("An error occured during operation");
            var result = await _repo.GetByGuid(guid);
            if (result != null)
            {
                return Ok(result);
            }
            return BadRequest("An error occured during operation");
        }
        
        [HttpPost, Route("AddNew")]
        public async Task<IActionResult> AddNewProduct([FromBody] ProductForCreateDto product)
        {
            var newProduct = _mapper.Map<Product>(product);
            newProduct.Id = new Guid();
            _repo.AddNewRecord(newProduct);

            if (await _repo.SaveAll())
                return Ok(new CreatedProductForReturn(newProduct.Id.ToString()));

            return BadRequest("An error occured during operation");
        }

        [HttpPut, Route("UpdateProduct")]
        public async Task<IActionResult> UpdateProduct(ProductForUpdateDto productForUpdateDto)
        {
            if (!GuidHelper.ValidateGuid(productForUpdateDto.Id.ToString()))
                return BadRequest("An error occured during operation");
            
            var productFromRepo = await _repo.GetByGuid(productForUpdateDto.Id.ToString());
            _mapper.Map(productForUpdateDto, productFromRepo);

            if (productFromRepo != null && await _repo.SaveAll())
            {
                return NoContent();
            }
                
            throw new Exception($"$Updating product {productForUpdateDto.Id} failed on save");
        }
        
        [HttpDelete, Route("DeleteProduct/{guid}")]
        public async Task<IActionResult> DeleteProduct(string guid)
        {
            if (!GuidHelper.ValidateGuid(guid))
                return BadRequest("An error occured during operation");
            
            var result = await _repo.DeleteByGuid(guid);

            if (result && await _repo.SaveAll())
            {
                return NoContent();
            }
            return BadRequest("An error occured during operation");
        }
    }
}